VBoxmanage import empty.ova 
VBoxmanage modifyvm "empty" --name okd4-bootstrap
VBoxmanage modifyvm "okd4-bootstrap" --macaddress1 080027C5EE79
VBoxmanage modifyvm "okd4-bootstrap" --memory 4096

VBoxmanage import empty.ova
VBoxmanage modifyvm "empty" --name okd4-master-1
VBoxmanage modifyvm "okd4-master-1" --macaddress1 08002764D665
VBoxmanage modifyvm "okd4-master-1" --memory 4096

VBoxmanage import empty.ova
VBoxmanage modifyvm "empty" --name okd4-master-2
VBoxmanage modifyvm "okd4-master-2" --macaddress1 080027E252DD
VBoxmanage modifyvm "okd4-master-2" --memory 4096

VBoxmanage import empty.ova
VBoxmanage modifyvm "empty" --name okd4-master-3
VBoxmanage modifyvm "okd4-master-3" --macaddress1 0800279C4F3D
VBoxmanage modifyvm "okd4-master-3" --memory 4096

VBoxmanage import empty.ova
VBoxmanage modifyvm "empty" --name okd4-worker-1
VBoxmanage modifyvm "okd4-worker-1" --macaddress1 080027A1F6DB
VBoxmanage modifyvm "okd4-worker-1" --memory 4096

VBoxmanage import empty.ova
VBoxmanage modifyvm "empty" --name okd4-worker-2
VBoxmanage modifyvm "okd4-worker-2" --macaddress1 0800273280C2
VBoxmanage modifyvm "okd4-worker-2" --memory 4096
